#!/usr/bin/env iocsh.bash

require(mrfioc2)

iocshLoad("./iocsh/env-init.iocsh")

epicsEnvSet("SYS", "Utg-Det:TS")
epicsEnvSet("IOC", "$(SYS)")
epicsEnvSet("PCI_SLOT", "1:0.0")
epicsEnvSet("PCIID", "$(PCI_SLOT)")
epicsEnvSet("DEVICE", "EVR-01")
epicsEnvSet("DEV", "$(DEVICE)")
epicsEnvSet("EVR", "$(DEVICE)")
epicsEnvSet("BUFFSIZE", "100")
epicsEnvSet("MRF_HW_DB", "evr-pcie-300dc-ess.db")
epicsEnvSet("E3_MODULES", "/epics/iocs/e3")
epicsEnvSet("EPICS_CMDS", "/epics/iocs/cmds")
epicsEnvSet("INTREF","")

# Find the PCI bus number for the cards in the crate
#system("$(EPICS_CMDS)/$(IOC)/find_pci_bus_id.bash")
#< "$(EPICS_CMDS)/$(IOC)/pci_bus_id"
#system("./find_pci_bus_id.bash")
#< "./pci_bus_id"


# Load e3-common
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")


iocshLoad("./iocsh/evr-pcie-300dc-init.iocsh", "S=$(IOC), DEV=$(DEV), PCIID=$(PCIID)")
#mrmEvrSetupPCI("$(EVR)", "$(PCI_BUS_NUM):0.0"))
#dbLoadRecords("$(MRF_HW_DB)","EVR=$(EVR),SYS=$(SYS),D=$(DEVICE),FEVT=88.0525,PINITSEQ=0")

dbLoadRecords("evrevent.db","EN=$(SYS)-$(DEVICE):EvtI, OBJ=$(DEVICE), CODE=18, EVNT=18"))
dbLoadRecords("evrevent.db","EN=$(SYS)-$(DEVICE):EvtJ, OBJ=$(DEVICE), CODE=19, EVNT=19"))
dbLoadRecords("evrevent.db","EN=$(SYS)-$(DEVICE):EvtK, OBJ=$(DEVICE), CODE=20, EVNT=20"))


# The amount of time which the EVR will wait for the 1PPS event before going into error state.
#var(evrMrmTimeNSOverflowThreshold, 1000000)

iocInit()

iocshLoad("./iocsh/evr-run.iocsh", "IOC=$(IOC), DEV=$(DEV)")

# Define 14Hz event by dividing event clock and triggering the sequencer (WORKING)
# Notice that the count is different to compensate for 88.05194802 MHz event clock in standalone mode
#dbpf $(SYS)-$(DEVICE):PS0-Div-SP 6289424 # 6289464
#dbpf $(SYS)-$(DEVICE):PS1-Div-SP 88052500 # 1 PPS

######### INPUTS #########

# Set up of UnivIO 0 as Input. Generate Code 10 locally on rising edge.
dbpf $(SYS)-$(DEVICE):In0-Lvl-Sel "Active High"
dbpf $(SYS)-$(DEVICE):In0-Edge-Sel "Active Rising"
dbpf $(SYS)-$(DEVICE):OutFPUV00-Src-SP 61
#dbpf $(SYS)-$(DEVICE):In0-Trig-Ext-Sel "Edge"
dbpf $(SYS)-$(DEVICE):In0-Trig-Ext-Sel "Off"
dbpf $(SYS)-$(DEVICE):In0-Code-Ext-SP 10
dbpf $(SYS)-$(DEVICE):EvtA-SP.OUT "@OBJ=$(EVR),Code=10"
dbpf $(SYS)-$(DEVICE):EvtA-SP.VAL 10

# Set up of UnivIO 1 as Input. Generate Code 11 locally on rising edge.
dbpf $(SYS)-$(DEVICE):In1-Lvl-Sel "Active High"
dbpf $(SYS)-$(DEVICE):In1-Edge-Sel "Active Rising"
dbpf $(SYS)-$(DEVICE):OutFPUV01-Src-SP 61
#dbpf $(SYS)-$(DEVICE):In1-Trig-Ext-Sel "Edge"
dbpf $(SYS)-$(DEVICE):In1-Trig-Ext-Sel "Off"
dbpf $(SYS)-$(DEVICE):In1-Code-Ext-SP 11
dbpf $(SYS)-$(DEVICE):EvtB-SP.OUT "@OBJ=$(EVR),Code=11"
dbpf $(SYS)-$(DEVICE):EvtB-SP.VAL 11


dbpf $(SYS)-$(DEVICE):EvtC-SP.OUT "@OBJ=$(EVR),Code=12"
dbpf $(SYS)-$(DEVICE):EvtC-SP.VAL 12
dbpf $(SYS)-$(DEVICE):EvtD-SP.OUT "@OBJ=$(EVR),Code=13"
dbpf $(SYS)-$(DEVICE):EvtD-SP.VAL 13
dbpf $(SYS)-$(DEVICE):EvtE-SP.OUT "@OBJ=$(EVR),Code=14"
dbpf $(SYS)-$(DEVICE):EvtE-SP.VAL 14
dbpf $(SYS)-$(DEVICE):EvtF-SP.OUT "@OBJ=$(EVR),Code=15"
dbpf $(SYS)-$(DEVICE):EvtF-SP.VAL 15
dbpf $(SYS)-$(DEVICE):EvtG-SP.OUT "@OBJ=$(EVR),Code=16"
dbpf $(SYS)-$(DEVICE):EvtG-SP.VAL 16
dbpf $(SYS)-$(DEVICE):EvtH-SP.OUT "@OBJ=$(EVR),Code=17"
dbpf $(SYS)-$(DEVICE):EvtH-SP.VAL 17


######### OUTPUTS #########
dbpf $(SYS)-$(DEVICE):DlyGen0-Width-SP 10 #2.86ms
#dbpf $(SYS)-$(DEVICE):DlyGen0-Delay-SP 0 #0ms
dbpf $(SYS)-$(DEVICE):DlyGen0-Evt-Trig0-SP 15
dbpf $(SYS)-$(DEVICE):OutFPUV04-Src-SP 0 #Connect to DlyGen-0


#Define 14Hz event with a delay of 10ms (WORKING)
dbpf $(SYS)-$(DEVICE):DlyGen1-Width-SP 10 #10us
#dbpf $(SYS)-$(DEVICE):DlyGen1-Delay-SP 0 #0ms
dbpf $(SYS)-$(DEVICE):DlyGen1-Evt-Trig0-SP 15
dbpf $(SYS)-$(DEVICE):OutFPUV05-Src-SP 1 #Connect to DlyGen-1

# Set up output
dbpf $(SYS)-$(DEVICE):DlyGen2-Width-SP 10 
#dbpf $(SYS)-$(DEVICE):DlyGen2-Delay-SP 0 
dbpf $(SYS)-$(DEVICE):DlyGen2-Evt-Trig0-SP 14
dbpf $(SYS)-$(DEVICE):OutFPUV06-Src-SP 2 #Connect to DlyGen-2

# Set up output
dbpf $(SYS)-$(DEVICE):DlyGen3-Width-SP 10 
#dbpf $(SYS)-$(DEVICE):DlyGen3-Delay-SP 0 
dbpf $(SYS)-$(DEVICE):DlyGen3-Evt-Trig0-SP 125
dbpf $(SYS)-$(DEVICE):OutFPUV07-Src-SP 3 

#epicsThreadSleep 1
#Setup of sequence for event code 14 generation
dbpf $(SYS)-$(DEVICE):SoftSeq0-RunMode-Sel 0
dbpf $(SYS)-$(DEVICE):SoftSeq0-TrigSrc-2-Sel "Prescaler 0"
dbpf $(SYS)-$(DEVICE):SoftSeq0-TsResolution-Sel 0 #Ticks
dbpf $(SYS)-$(DEVICE):SoftSeq0-Load-Cmd 1
dbpf $(SYS)-$(DEVICE):SoftSeq0-Enable-Cmd 1
system("/bin/sh ./config_seq_14Hz.sh $(SYS) $(DEVICE)")
#epicsThreadSleep 2
dbpf $(SYS)-$(DEVICE):SoftSeq0-Commit-Cmd 1
#EOF
